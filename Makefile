CROSS_COMPILE=arm-none-linux-gnueabi-
COMPILER=$(CROSS_COMPILE)g++ -W -Wextra -Wformat=2 -Wall

PATH_MAGICLUAINTERFACE:=/home/paxdev/pax-dev-container/source/LuaMagicInterface
LDFLAGS_MAGICLUAINTERFACE:=-L$(PATH_MAGICLUAINTERFACE)/bin -lmagicluainterface
INCLUDES_MAGICLUAINTERFACE:=-I$(PATH_MAGICLUAINTERFACE)

PATH_BARCODELIB:=../libbarcode-1.3.6898_SIG
LDFLAGS_BARCODELIB:=-L$(PATH_BARCODELIB)/lib
INCLUDES_BARCODELIB:=-I$(PATH_BARCODELIB)/include

PATH_TOOLCHAIN:=/home/paxdev/sdk/platforms/paxngfp_201205
LDFLAGS_TOOLCHAIN:=-L$(PATH_TOOLCHAIN)/lib/ -losal -Wl,-rpath=//opt/lib -Wl,-rpath=./lib -Wl,-rpath-link,"$(PATH_TOOLCHAIN)/lib" -lmagicluainterface -lts -lpng  -lz -lfreetype -lssl -lcrypto  -lm -lpthread -lxui -lrt
INCLUDES_TOOLCHAIN:=-I$(PATH_TOOLCHAIN)/include

ALL:
	$(COMPILER) demo_barcode.c $(INCLUDES_TOOLCHAIN) $(INCLUDES_MAGICLUAINTERFACE) $(LDFLAGS_MAGICLUAINTERFACE) $(LDFLAGS_TOOLCHAIN) -o ricky.out




