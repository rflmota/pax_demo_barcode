#incldue < prolin_barcode_lib.h >
#include <exportfunctions.h>

int main(int argc, char const *argv[])
{
    // root = XuiRootCanvas();
    // XuiColor color;
    // color.r = 127; color.b = 127; color.g = 127;
    // XuiCanvasSetBackground(root, XUI_BG_NORMAL, NULL, color);

    MagicInitLib((char *)'i');

    MagicDispMsg("TITLE", "MESSAGE", '1', 2000);

    ST_ENCODED_INFO BeEncodedInfo = {
        .Type = QRCODE,
        .String = "www.petrotec.com",
        .Len = strlen("www.petrotec.com"),
        .SizeLevel = 1,
        .CorrectionLevel = 1,
        .Column = 1,
    };

    ST_BITMAP Bitmap = {0};

    Bitmap.Data = malloc(300*400*4);
    Bitmap.Size = 300*400*4;
    memset(Bitmap.Data, 0, Bitmap.Size);

    OsBarcodeGetBitmap(&BeEncodedInfo, &Bitmap);

    bmp_size = ((((Bitmap.Width*Pixel)+7)/8+2)*Bitmap.Height*Pixel+1)*sizeof(unsigned char);

    Barcode_2D = malloc(bmp_size);
    memset(Barcode_2D, 0, Size);
    Convert2DPrinterDataPixel(&Bitmap, Pixel, Barcode_2D, Size);

    return 0;
}
