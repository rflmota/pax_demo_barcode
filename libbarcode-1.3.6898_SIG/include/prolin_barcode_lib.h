#ifndef __PAX_BARCODE_LIB__
#define __PAX_BARCODE_LIB__


#define MODULE_SIZE_BASE   1
#define MODULE_SIZE_TINY   2
#define MODULE_SIZE_SMALL  4
#define MODULE_SIZE_MEDIUM 8
#define MODULE_SIZE_BIG    12
#define MODULE_SIZE_LARGE  16

#define ERR_INVALID_PARAM  	(-1003)	/*invalid param*/
#define ERR_STR_LEN			(-1016)	/* String is too long */
#define ERR_MEM_FAULT		(-1013)	/* Memory error */


typedef struct {
	int Width;
	int Height;
	int Size;
	unsigned char *Data;
} ST_BITMAP;

typedef enum {	
	QRCODE,
	PDF417, 
	GRIDMATRIX,
	CODE128,
	CODE39,
	EAN13,
	EAN128, 
	AZTEC,
	ITF,
}ENCODED_TYPE;

typedef struct {
	ENCODED_TYPE Type;
	char *String;
	int Len;
	int SizeLevel;
	int CorrectionLevel;
	int Column;
	int AztecSize;
	char ExpParam[64];
} ST_ENCODED_INFO;


int OsBarcodeGetBitmap(ST_ENCODED_INFO * BeEncodedInfo, ST_BITMAP * Bitmap);


#endif  /* __PAX_BARCODE_LIB__ */

